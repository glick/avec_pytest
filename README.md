# Avec pytest

Techniques de test avancées en Python


## Plan détaillé
1.    Gérer les outils de tests  
  doctest
1.    Ecrire les tests  
		parametrize
		TDD
		setup teardown fixture
		fuzzing hypothesis
1.    Trouver les tests  
		--collect-only
		-k
		-m
1.    Exécuter les tests  
		tous, certains, un test
		pytest-sugar  
		perf
			pytest-xdist
1.    Produire un rapport  
		pytest-cov
		JUnit.xml
		pytest-html
		pytest-cpp
		allure-pytest  
		Les plugins
			pytest-instafail
			pytest-clarity
			pytest-picked

### Remarque
Pour avoir une image en plein écran dans le fond, on peut remplacer une balise img
```<img src="images/allure_tab_overview.png" />``` par
```<!-- .slide: data-background-image="images/allure_tab_overview.png" data-background-size="100% 100%" data-background-position="center" data-background=" " data-background-repeat=" " data-background-transition="none" -->```


## Mise au point

Cette commande permet de recharger automatiquement les slides après une modification du fichier markdown source : 

```sudo docker run --rm -p 1948:1948 -p 35729:35729 -v `pwd`:/slides webpronl/reveal-md:latest /slides --watch --title "Avec pytest" --theme white```

Les slides sont à http://localhost:1948/

Ctrl+c pour arrêter pour arrêter le serveur.


# Raccourcis Reveal.js

- `S` : speaker view, vue pour le présentateur avec chrono, slide suivant et notes
- `Esc` ou `O` : Overview, survol des slides
- `g` : goto slide number, aller au slide numéro colonne/ligne
- `f` : fullscreen, plein écran
- `Ctrl+f` : find in page, rechercher dans la page courante
- `:` : écran noir
