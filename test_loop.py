# standard 
import time

# external
import pytest

# local


@pytest.mark.parametrize("number", range(-1,8))
def test_positive(number):
    time.sleep(1)
    if number in [-1, 0]:
        pytest.xfail()  # expected failed so the pipeline won't failed
    assert number > 0
