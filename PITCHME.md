
# Avec pytest

Techniques de test avancées en Python

Gaël Lickindorf - Consultant DevOps NeoSoft

24 janvier 2019

----

## Introduction

Présentation en ligne

https://gitlab.com/glick/avec_pytest

----

### Pour Qui ?

- product owner (critères/tests d'acceptance)
- développeurs (analyse statique, tests unitaires, doc testable)
- testeurs (tests fonctionnels)
- devops (CI/CD pipeline, GitLab CI, Jenkins ...)
- chef de projet, responsable AQ (rapports)

----

### Qu'est-ce que pytest ?

<img src="images/anneau_entre_les_doigts_2.gif" height="195"/> <!-- .element: class="fragment" -->

----

### Citation

Gandalf le Gris:  <!-- .element: class="fragment" -->  
>One Ring to rule them all, <!-- .element: class="fragment" -->  
>One Ring to find them, <!-- .element: class="fragment" -->  
>One Ring to bring them all <!-- .element: class="fragment" -->  
>and in the darkness bind them <!-- .element: class="fragment" -->  

Note:
- Un Anneau pour les gouverner tous
- Un Anneau pour les trouver
- Un Anneau pour les amener tous,
- Et dans les ténèbres les lier

----

### Plan en Français

Pytest: outil de test *pythonique*  
en ligne de commande  <!-- .element: class="fragment" -->  

1. Gérer les outils de tests <!-- .element: class="fragment" -->  
2. (Ecrire les tests) <!-- .element: class="fragment" -->  
3. Trouver les tests <!-- .element: class="fragment" -->  
4. Exécuter les tests <!-- .element: class="fragment" -->  
5. Produire un rapport <!-- .element: class="fragment" -->  

----

### Préambule: Histoire

Autrefois dans la Comté pytest s'écrivait py.test

L'heure est venue le 25 novembre 2010  
l'anneau est sorti de la poche de Bilbon  

py.test est devenu pytest 2.0  
un package indépendant 
de la librarie [py](https://pypi.org) 1.4

----

### Prérequis

Python, pip, accès Internet|packages python


Installation
```bash
pip install pytest
...
```

Note:
Hors scope: venv tox

--------------------------------------------------------------------------------

---
## 1. Gérer les Outils de test

----
### Taxonomie

wiki python: [PythonTestingToolsTaxonomy](https://wiki.python.org/moin/PythonTestingToolsTaxonomy)

12 catégories

tests unitaires:  
pytest > nose2 > ~~nose~~ > unittest  
doctest  

Note:
pytest can launch tests made for others tools
```python
class IsOddTestCase(unittest.TestCase):
     def test_is_not_odd_4(self):
        number = Number(4)
        result = number.is_odd()
        self.assertTrue(result, "4 should be odd")
```

----

### Code before Doctests

```python
class Number():

    def __init__(self, number):
        self.number = number

    def ceil(self):
        """Rounds a number UP to the nearest integer,
        if necessary, and returns the result.
        """
        if self.number % 1 >= 0:
            return int(self.number) + 1
```

----

### Doctests késako

From the python documentation:

>The doctest module searches for pieces of text that look like interactive Python sessions, and then executes those sessions to verify that they work exactly as shown.

----

### First doctests

```python
    def ceil(self):
        """Rounds a number UP to the nearest integer,
        if necessary, and returns the result.
        >>> Number(2.5).ceil()
        3
        >>> import math
        >>> Number(math.pi).ceil()
        4
        """
        if self.number % 1 >= 0:
            return int(self.number) + 1
```

Note:
- ceiling : plafond
- arrondi à l'entier supérieur ou égal

----

### Launch it

```bash
$> python -m doctest main.py
$> 
```
Nothing to declare means Success ! <!-- .element: class="fragment" --> 

```bash
$> echo $?
0
```
<!-- .element: class="fragment" --> 

----

### with details: --verbose or -v

```python
$> python -m doctest -v main.py
Trying:
    Number(2.5).ceil()
Expecting:
    3
ok
Trying:
    import math
Expecting nothing
ok
Trying:
    Number(math.pi).ceil()
Expecting:
    4
ok
...
3 passed and 0 failed.
Test passed.
```

----

### With pytest

```python
$> pytest --doctest-modules -v main.py
===================== test session starts =====================
...
collected 1 item

main.py::main.Number.ceil PASSED                         [100%]

================== 1 passed in 0.03 seconds ===================
```

----

### Small problem

What is the ceiling of an integer?

```python
>>> import math
>>> math.ceil(3)
3
>>> import main
>>> number = main.Number(3)
>>> number.ceil()
4
```

----

### Rule of tests

>Tests can only prove the presence of features, not the absence of bugs.

----

## Add a new test and fix the code!

----

### New test

```python
def test_ceil_int():
    number = Number(3)
    assert number.ceil() == 3
```

----

### Main.py

```python
    def ceil(self):
        """Rounds a number UP to the nearest integer,
        if necessary, and returns the result.
        >>> Number(2.5).ceil()
        3
        >>> import math
        >>> Number(math.pi).ceil()
        4
        """
        if self.number % 1 > 0:
            return int(self.number) + 1
        return self.number
```

--------------------------------------------------------------------------------

---
## 2. Ecrire les tests

- TDD
- fixture
- parametrize
- fuzzing with hypothesis

---
### TDD is not dead

<img src="images/tdd_flow.gif" />

----

### get_sign method

Let's add test for a non-existing test_sign method that must follow these requirements:

* Number(1).get_sign() == '+'
* Number(-1).get_sign() == '-'
* Number(0).get_sign() == '+'

----

### get_sign tests

```python
def test_get_sign_1():
    assert Number(1).get_sign() == '+'

def test_get_sign_minus_1():
    assert Number(-1).get_sign() == '-'

def test_get_sign_zero():
    assert Number(0).get_sign() == '+'
```

----

### Launch it

```python
_____________ test_get_sign_1 _____________________________________

    def test_get_sign_1():
>       assert Number(1).get_sign() == '+'
E       AttributeError: 'Number' object has no attribute 'get_sign'

tests/test_number.py:49: AttributeError
```

----

### A small step at the time

```python
class Number():

    def get_sign(self):
        pass
```

----

### Relaunch the tests

```python
____________________________________ test_get_sign_1 _____________________________________

    def test_get_sign_1():
>       assert Number(1).get_sign() == '+'
E       assert None == '+'
E        +  where None = <bound method Number.get_sign of <main.Number object at 0x10f00bc90>>()
E        +    where <bound method Number.get_sign of <main.Number object at 0x10f00bc90>> = <main.Number object at 0x10f00bc90>.get_sign
E        +      where <main.Number object at 0x10f00bc90> = Number(1)

tests/test_number.py:49: AssertionError
```

----

### Fix the code!

----

### Code

```python
class Number():

    def get_sign(self):
        if self.number >= 1:
            return "+"
        else:
            return "-"
```

----

## Result

```
...
tests/test_number.py::test_get_sign_1 PASSED
tests/test_number.py::test_get_sign_minus_1 PASSED
tests/test_number.py::test_get_sign_zero FAILED
...
```

----

### Result

```python
___________________________________ test_get_sign_zero ___________________________________

    def test_get_sign_zero():
>       assert Number(0).get_sign() == '+'
E       assert '-' == '+'
E         - -
E         + +

tests/test_number.py:57: AssertionError
```

----

### Fix the code

----

### Fixed code

```python
class Number():

    def get_sign(self):
        if self.number <= -1:
            return "-"
        else:
            return "+"
```

----

### Result

```
...
tests/test_number.py::test_get_sign_1 PASSED
tests/test_number.py::test_get_sign_minus_1 PASSED
tests/test_number.py::test_get_sign_zero PASSED
...
```

----

### What is the use-case of TDD?

* Think about API before implementation.
* Helps design testables piece of code.
* Usually implement fully tested piece of code (100% code coverage.)

---
### Fixture intro: Anatomie d'un test

```python
    setup()

    def test_something():
        execution()
        assertions()

    teardown()
```

----
### Setup teardown => fixture

classique: setup session module class fonction

=> construire un paramètre: fixture

----
### fixture: exemple

```python
import pytest

@pytest.fixture
def smtp_connection():
    import smtplib
    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)

def test_ehlo(smtp_connection):
    response, msg = smtp_connection.ehlo()
    assert response == 250
```

---
### parametrize intro: test loop

Test severals numbers at the same time

```python
    def test_first_ten_numbers():
        for i in range(10):
            number = Number(i * 1)
            result = number.is_odd()
            assert result
```

Note:
odd : impair

----

### Result

```python
tests/test_number.py F                                                   [100%]

=================================== FAILURES ===================================
____________________________ test_first_ten_numbers ____________________________

    def test_first_ten_numbers():
        for i in range(10):
            number = Number(i * 1)
            result = number.is_odd()
>           assert result
E           assert False

tests/test_number.py:7: AssertionError
=========================== 1 failed in 0.03 seconds ===========================
```

----

### Rule of tests

>Don't use loops to test several cases! NEVER!

----

### Instead leverage your test framework

Using `pytest.mark.parametrize`

```python
import pytest

@pytest.mark.parametrize("number", range(10))
def test_first_ten_numbers(number):
    number = Number(number * 1)

    result = number.is_odd()

    assert result is False
```

----

### Output

```
tests/test_number.py::IsOddTestCase::test_is_not_odd_4 PASSED
tests/test_number.py::IsOddTestCase::test_is_odd_1 PASSED
tests/test_number.py::test_first_ten_numbers[0] PASSED
tests/test_number.py::test_first_ten_numbers[1] FAILED
tests/test_number.py::test_first_ten_numbers[2] PASSED
tests/test_number.py::test_first_ten_numbers[3] FAILED
tests/test_number.py::test_first_ten_numbers[4] PASSED
tests/test_number.py::test_first_ten_numbers[5] FAILED
tests/test_number.py::test_first_ten_numbers[6] PASSED
tests/test_number.py::test_first_ten_numbers[7] FAILED
tests/test_number.py::test_first_ten_numbers[8] PASSED
tests/test_number.py::test_first_ten_numbers[9] FAILED
```

----

```
======================================== FAILURES ========================================
_______________________________ test_first_ten_numbers[1] ________________________________

number = <main.Number object at 0x107e11510>

    @pytest.mark.parametrize("number", range(10))
    def test_first_ten_numbers(number):
        number = Number(number * 1)

        result = number.is_odd()

>       assert result is False
E       assert True is False

tests/test_number.py:39: AssertionError
```

----

### Fix the test

```python
@pytest.mark.parametrize("number", range(10))
def test_first_ten_numbers(number):
    number = Number(number * 2)

    result = number.is_odd()

    assert result is False
```

----

### And rerun

```python
tests/test_number.py::IsOddTestCase::test_is_not_odd_4 PASSED
tests/test_number.py::IsOddTestCase::test_is_odd_1 PASSED
tests/test_number.py::test_first_ten_numbers[0] PASSED
tests/test_number.py::test_first_ten_numbers[1] PASSED
tests/test_number.py::test_first_ten_numbers[2] PASSED
tests/test_number.py::test_first_ten_numbers[3] PASSED
tests/test_number.py::test_first_ten_numbers[4] PASSED
tests/test_number.py::test_first_ten_numbers[5] PASSED
tests/test_number.py::test_first_ten_numbers[6] PASSED
tests/test_number.py::test_first_ten_numbers[7] PASSED
tests/test_number.py::test_first_ten_numbers[8] PASSED
tests/test_number.py::test_first_ten_numbers[9] PASSED
```

----
### With expected result

```python
# test_parametrize.py
import pytest
@pytest.mark.parametrize("test_input,expected", [
    ("3+5", 8),
    ("2-4", -2),
    ("6*9", 42),
])
def test_eval(test_input, expected):
    assert eval(test_input) == expected
```

----

```bash
$  pytest -v test_parametrize.py
================================== test session starts ===================================
platform win32 -- Python 3.6.8, pytest-4.1.1, py-1.7.0, pluggy-0.8.1 -- c:\users\lickindo\.
virtualenvs\pytest-adgyi6hr\scripts\python.exe
cachedir: .pytest_cache
rootdir: C:\gael\src\pytest\parametrize, inifile:
plugins: allure-pytest-2.5.4
collected 3 items

test_parametrize.py::test_eval[3+5-8] PASSED     [ 33%]
test_parametrize.py::test_eval[2-4--2] PASSED    [ 66%]
test_parametrize.py::test_eval[6*9-42] FAILED    [100%]
[...]
```

----
```bash
======================================== FAILURES ========================================
___________________________________ test_eval[6*9-42] ____________________________________
test_input = '6*9', expected = 42
    @pytest.mark.parametrize("test_input,expected", [
        ("3+5", 8),
        ("2-4", -2),
        ("6*9", 42),
    ])
    def test_eval(test_input, expected):
>       assert eval(test_input) == expected
E       assert 54 == 42
E         -54
E         +42
test_parametrize.py:8: AssertionError
=========================== 1 failed, 2 passed in 0.06 seconds ===========================
```

---
### Fuzzing

Calling a piece of code with very various inputs to find defects.

----

### Fuzzing in python

```bash
pip install hypothesis
```

----

### Let's take a new method

```python
class Number():

    def divisible_by_11():
        """A number is divisible by 11 if and only if the alternating (in sign)
        sum of the number’s digits is 0.
        """
        string_number = str(self.number)
        alternating_sum = sum([(-1) ** i * int(d) for i, d
                               in enumerate(string_number)])
        return alternating_sum == 0
```

----

### Test it traditionnaly

```python
@pytest.mark.parametrize("number", range(1, 10))
def test_is_divisible_by_11(number):
    assert Number(number * 11).divisible_by_11() == True

    assert Number(number * 11 + 1).divisible_by_11() == False
```

----

### Output

```
...
tests/test_number.py::test_is_divisible_by_11[1] PASSED
tests/test_number.py::test_is_divisible_by_11[2] PASSED
tests/test_number.py::test_is_divisible_by_11[3] PASSED
tests/test_number.py::test_is_divisible_by_11[4] PASSED
tests/test_number.py::test_is_divisible_by_11[5] PASSED
tests/test_number.py::test_is_divisible_by_11[6] PASSED
tests/test_number.py::test_is_divisible_by_11[7] PASSED
tests/test_number.py::test_is_divisible_by_11[8] PASSED
tests/test_number.py::test_is_divisible_by_11[9] PASSED
...
```

----

### Fuzz it

```python
from hypothesis import given  # This is how we will define inputs
from hypothesis.strategies import integers  # This is the type of input we will use

@given(number=integers(min_value=1))  # This is the main decorator
def test_divisible_by_11(number):
    assert Number(11 * number).divisible_by_11() == True
```

----

### Output

```python
number = 19

    @given(number=integers(min_value=1))  # This is the main decorator
    def test_divisible_by_11(number):
>       assert Number(11 * number).divisible_by_11() == True
E       assert False == True
E        +  where False = <bound method Number.divisible_by_11 of <main.Number object at 0x107288b10>>()
E        +    where <bound method Number.divisible_by_11 of <main.Number object at 0x107288b10>> = <main.Number object at 0x107288b10>.divisible_by_11
E        +      where <main.Number object at 0x107288b10> = Number((11 * 19))

tests/test_number.py:71: AssertionError
------------------------------------------------------------------------------------ Hypothesis -------------------------------------------------------------------------------------
Falsifying example: test_divisible_by_11(number=19)
```

----

### Failing test

Hypothesis give us an example: `Falsifying example: test_divisible_by_11(number=19)`.

`19×11=209` and it should be divisible by 11 by construction, we have a bug!

---
### Mock

- unit test, isolation, dependencies
- fake component, function, control return value

----
### When to mock ?

- unpredictable (live data, random)
- slow, too long
- unavailable dependencie
  - temporary => integration, validation
- simulate error

----
### Risks

hide reality:  
- integration errors
- evolutions
- production data

----
### monkeypatch fixture

to modify objects, dictionaries or os.environ:
```python
monkeypatch.setattr(obj, name, value, raising=True)
monkeypatch.delattr(obj, name, raising=True)
monkeypatch.setitem(mapping, name, value)
monkeypatch.delitem(obj, name, raising=True)
monkeypatch.setenv(name, value, prepend=False)
monkeypatch.delenv(name, raising=True)
monkeypatch.syspath_prepend(path)
monkeypatch.chdir(path)
```

----
### monkeypatch example

```python
import os.path
def getssh(): # pseudo application code
    return os.path.join(os.path.expanduser("~admin"), '.ssh')

def test_mytest(monkeypatch):
    def mockreturn(path):
        return '/abc'
    monkeypatch.setattr(os.path, 'expanduser', mockreturn)
    # monkeypatch.setattr(os.path, 'expanduser', lambda path: '/abc')
    x = getssh()
    assert x == '/abc/.ssh'
```

----
### Mock AWS

- [PyPI pytest-aws](https://pypi.org/project/pytest-aws/) => <i class="fa fa-github" aria-hidden="true"></i> [pytest-services](https://github.com/mozilla-services/pytest-services)
- mock: moto, placebo
- pytest-moto ?
- pytest-localstack
- AWS Serverless Application Model: [AWS SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html)
- pytest-dynamodb
- [aioboto3](https://github.com/terrycain/aioboto3): boto3 resources with the [aiobotocore](https://github.com/aio-libs/aiobotocore) async backend

--------------------------------------------------------------------------------

---
## 3. Trouver les tests

----
### Juste Sélectionner

```bash 
pytest --collect-only
```

----
### Trouver les tests par mots clés

```bash 
pytest -k mot  
pytest -k "mot_inclus and not mot_exclu"
```

----
### Marquer c'est grouper

Marquer c'est tagger
```python
import pytest

@pytest.mark.add
def test_something(self):
    assertions()
```

```bash 
pytest -m add
```

----
### Exemples de marque :)

<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ```pytest -v -m NR -m Endurance```  
=> ```pytest -v -m Endurance```
```bash
pytest -v  -m "NR or Endurance"
pytest -v  -m "NR and Endurance"
pytest -v  -m "NR and not Endurance"
pytest -v --collect-only -m "not NR and not Endurance"
pytest -v  -m "not NR and not Endurance and not skip"
```
----
### Documenter vos marques

```
# content of pytest.ini
[pytest]
markers =
    NR: Non Regression test.
```

----
### Bonus 1: list existing markers

```
$> pytest --markers
@pytest.mark.NR: Non Regression test.

@pytest.mark.filterwarnings(warning): add a warning filter to the given test. see https://docs.pytest.org/en/latest/warnings.html#pytest-mark-filterwarnings 

@pytest.mark.skip(reason=None): skip the given test function with an optional reason. Example: skip(reason="no way of currently testing this") skips the test.

@pytest.mark.skipif(condition): skip the given test function if eval(condition) results in a True value.  Evaluation happens within the module global context. Example: skipif('sys.platform == "win32"') skips the test if we are on the win32 platform. see https://docs.pytest.org/en/latest/skipping.html

@pytest.mark.xfail(condition, reason=None, run=True, raises=None, strict=False): mark the test function as an expected failure if eval(condition) has a True value. Optionally specify a reason for better reporting and run=False if you don't even want to execute the test function. If only specific exception(s) are expected, you can list them in raises, and if the test fails in other ways, it will be reported as a true failure. See https://docs.pytest.org/en/latest/skipping.html

@pytest.mark.parametrize(argnames, argvalues): call a test function multiple times passing in different arguments in turn. argvalues generally needs to be a list of values if argnames specifies only one name or a list of tuples of values if argnames specifies multiple names. Example: @parametrize('arg1', [1,2]) would lead to two calls of the decorated test function, one with arg1=1 and another with arg1=2.see https://docs.pytest.org/en/latest/parametrize.html for more info and examples.

@pytest.mark.usefixtures(fixturename1, fixturename2, ...): mark tests as needing all of the specified fixtures. see https://docs.pytest.org/en/latest/fixture.html#usefixtures 

@pytest.mark.tryfirst: mark a hook implementation function such that the plugin machinery will try to call it first/as early as possible.

@pytest.mark.trylast: mark a hook implementation function such that the plugin machinery will try to call it last/as late as possible.
```
----
### Bonus 2: check for typos
--strict is case sensitive
```bash
$> pytest --strict --collect-only tests
============================= test session starts ==============================
...
collected 0 items / 1 errors                                                   

==================================== ERRORS ====================================
____________________ ERROR collecting tests/test_number.py _____________________
'Nr' not a registered marker
!!!!!!!!!!!!!!!!!!! Interrupted: 1 errors during collection !!!!!!!!!!!!!!!!!!!!
=========================== 1 error in 0.27 seconds ============================
$ echo $?
2
```

--------------------------------------------------------------------------------

---
## 4. Exécuter les tests


----
### Exécuter tous les tests

```bash
pytest
```

commande qui ajoute . dans PYTHONPATH et lance les tests:  
```bash
python -m pytest
```


----
### Exécuter seulement certains  tests

```
$ py.test tests/unit/
$ py.test toto.py 
$ py.test --pyargs mon.module 
```

----
### Un test bien précis

```
$ py.test fichier.py::fonction 
$ py.test fichier.py::Classe::methode 
```

----
### Variable on CLI v1: env

```bash
$ VAR='foo' pytest tests
```

```python
import os

def test_cli_variable():
   assert os.environ['VAR'] == 'foo'
```

----
### Variable on CLI v2: env + fixture

```python
# content of conftest.py
import os

import pytest

@pytest.fixture
def var():
    """Return the value of the environment variable var."""
    return os.environ.get('VAR','def')
```

```python
def test_cli_variable(var):
   assert var == 'foo'
``` 
<!-- .element: class="fragment" --> 

----
### Fixtures list

```bash
$ pytest --fixtures 
============================= test session starts ==============================
[...]
collected 4 items                                                              
cache
    Return a cache object that can persist state between testing sessions.
[...]
------------------------ fixtures defined from conftest ------------------------
var
    Return the value of the environment variable var.

========================= no tests ran in 0.03 seconds =========================
```

----
### Variable on CLI v3: option

```python
# content of conftest.py
import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--cmdopt", action="store", default="type1", 
        help="my option: type1 or type2"
    )

@pytest.fixture
def cmdopt(request):
    return request.config.getoption("--cmdopt")
```

----
### Run with default value

```bash
$ pytest
============================= test session starts =============================
[...]
test_sample.py F                                                         [100%]

================================== FAILURES ===================================
_________________________________ test_answer _________________________________

cmdopt = 'type1'

    def test_answer(cmdopt):
>       assert cmdopt == "type2"
E       AssertionError: assert 'type1' == 'type2'
E         - type1
E         ?     ^
E         + type2
E         ?     ^

test_sample.py:3: AssertionError
========================== 1 failed in 0.05 seconds ===========================
```

----
### Run with option

```bash
$ pytest -q --cmdopt type2
.                                                                                                                                                                                                                    [100%]
1 passed in 0.02 seconds
```

Help <!-- .element: class="fragment" -->  
```bash
$ pytest -h | grep cmdopt
  --cmdopt=CMDOPT       my option: type1 or type2
```
<!-- .element: class="fragment" --> 

----

### Launch it with a better test runner

```bash
pip install pytest pytest-sugar
```

le plugin sugar est activé par défaut
```bash
pytest tests -v
```

sans le plugin sugar
```bash
pytest tests -v -p no:sugar
```

----
### sugar: progressbar ...
<img src="images/pytest-sugar-video_v2.gif" />

----
### sugar in GitLab console

```bash
$ pytest --force-sugar
Test session starts (platform: linux, Python 3.11.4, pytest 7.4.0, pytest-sugar 0.9.7)
rootdir: /builds/glick/avec_pytest
plugins: sugar-0.9.7
collected 9 items
 test_loop.py xx✓✓✓✓✓✓✓                                          100% ██████████
Results (9.04s):
       7 passed
       2 xfailed
```

----
### and instafailing tests
<img src="images/pytest.png" />
<!-- .slide: data-background-image="images/pytest.png" data-background-size="100% 100%" data-background-position="center" data-background=" " data-background-repeat=" " data-background-transition="none" -->

----
### instafail Installation and Usage

- failed tests in real time
```bash
pip install pytest-instafail
```

- Usage:
```bash
pytest --instafail
pytest -v --failed-first --instafail
```

---

### Perf: Optimisation du temps d'exécution

- MESURER (log, monitoring, profiling ...)

- infra
  - matériel: nb CPU, Mémoire, disques SSD
  - réseau: débit ,proximité, cache, repo local
  - scalabilité, les conteneurs démarrent plus vite que les VMs

----
### Perf: Installation

  - cache au niveau des runners
  - cachedir: .pytest_cache
  - suppression des caches automatique (soir ou we)

----
### Perf: Sélection:
  - réduire les répertoires
  - en fonction du code source modifié
    - analyse statique
    - dépendances (code, modules, exécutables, architecture ...)
    - matrice de couverture
  - en fonction de la durée du test

----
### Perf: Exécution en parallèle
  - runners: VMs, containers
  - jobs: GitLab CI:
    - keyword: [parallel](https://docs.gitlab.com/ee/ci/yaml/README.html#parallel)
    - variables: CI_NODE_INDEX CI_NODE_TOTAL
  - pytest-xdist: plusieurs process et execution à distance

----
### Perf: Ordre, démarrage, arrêt

- tests ayant échoués en premier:
  - ```--failed-first```
- stratégie: `!` aux dépendances
  - setup, bdd, media ...

- Conditions d'arrêts:
  - timeout (projet=>job, pytest-timeout)
  - ```-x, --exitfirst``` exit instantly on first error
  - ```--maxfail=n``` exit after first n errors.

--------------------------------------------------------------------------------

---
## 5. Produire un rapport

----
### Couverture de code


```bash
pip install pytest-cov
```

----

### Lancer les tests avec couverture

```
$> py.test --doctest-modules tests main.py -v
...
-------- coverage: platform darwin, python 2.7.12-candidate-1 --------
Name      Stmts   Miss  Cover
-----------------------------
main.py       9      0   100%
```

----

### Rapport de couverture en HTML

```
$> py.test --doctest-modules tests main.py -v --cov=main --cov-report=html
```

----

<img src="images/coverage_html.png" />
<!-- .slide: data-background-image="images/coverage_html.png" data-background-size="100% 100%" data-background-position="center" data-background=" " data-background-repeat=" " data-background-transition="none" -->


----
### Rapport (JUnit).xml

- standard de fait
- ```pytest --junit-xml=build\junit.xml```
- integration avec GitLab
```yaml
  artifacts:
    paths:
      - build\junit.xml
    reports:
      junit: build/junit.xml
    expire_in: 1 week
    when: always
```

----
### Rapport Python et C++

pytest-cpp supports:
- both Google Test and Boost::Test
- but not Yaffut  
Yet Another Framework For (C++) Unit Testing

- ```--junitxml=build/unitTests/report.xml```

----
### Rapport html

plugin: pytest-html

options:
- ```--html=build/funcTests/report.html``` 
- ~~```--self-contained-html```~~


----
### Rapport avec Allure 

Installation
- Allure framework: https://docs.qameta.io/allure/
- plugin pytest: ```$ pip install allure-pytest```

Usage
```
$ pytest --alluredir=/tmp/my_allure_results
```

Visualisation
```
$ allure serve /tmp/my_allure_results
```

----
### Patch for NexGuard

- https://gitlab.kudelski.com/nexguard/Libs/Testing/allure2
- to display **junit.xml** system-out and system-err

- unzip allure-commandline-2.9-SNAPSHOT.zip
- launch **C++** tests to generate E:\report\junit.xml
- generate a html report and browse:  
  ```
  allure-2.9-SNAPSHOT\bin\allure serve E:\report
  ```
----
### Add Links in test overview

```python
import allure

@allure.link(url=LINK, name=LINK_NAME, link_type=LINK_TYPE)
@allure.testcase(TEST_CASE, name=TEST_CASE_NAME)
@allure.issue(ISSUE, name=ISSUE_NAME)
def test_with_links_cases_and_issues():
    assert True
```

----
### Allure demo

https://demo.qameta.io/allure/

----
<img src="images/allure_tab_overview.png" />

----
<img src="images/allure_testcase.png" />


---

## Les plugins

----
### La communauté de l'anneau

- 1289 plugins sur https://docs.pytest.org/en/latest/reference/plugin_list.html
- \+ de 10_000 projets "pytest" sur PyPI https://pypi.org/search/?q=pytest  

----
### Quelques plugins

- parallel & remote tests: pytest-xdist: -n auto
- diff stacktrace: pytest-clarity  
- git untracked, modified: pytest-picked
- PYTHONPATH set python_paths in python.ini : [pytest-pythonpath](https://github.com/bigsassy/pytest-pythonpath)
- common security issues: pytest-bandit

----
### Ecrire un plugin

[cookiecutter](https://github.com/audreyr/cookiecutter) : création de projet à partir de template

appliqué à la création de plugin pytest:  
https://github.com/pytest-dev/cookiecutter-pytest-plugin

---

## ?
----

### Merci

----

### Sources

- doc officielle: https://pytest.readthedocs.io

- PyCon Fr
 - 2016 [Python testing 101](https://github.com/Lothiraldan/python-test-101-pyconfr)
 - 2017 [Techniques de test avancées en Python](https://pyvideo.org/pycon-fr-2017/techniques-de-test-avancees-en-python.html)
 - 2018 [Tester mieux, tester moins, avec Hypothesis](https://www.youtube.com/watch?v=PnBbPmZ9mVg)

